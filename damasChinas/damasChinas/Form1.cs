﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace damasChinas
{
    public partial class btnRetro : Form
    {
        PictureBox current = null;
        int turno = 0;
        object[][] rojas;  // matriz para las fichas rojas
        object[][] azules; // matriz para las fichas azules
        bool nextMov = false; // indicador de turno
        List<object[]> blueKilled = new List<object[]>(); // matriz para las fichas que se ha comido del azul
        List<object[]> redKilled = new List<object[]>();  // matriz para las fichas que se ha comido del azul

        Pila pilaRoja = new Pila();
        Pila pilaAzul = new Pila();

        int[,] matrizDeTablero = new int[8, 8]
            {
                {01, 00, 01, 00, 01, 00, 01, 00},
                {00, 01, 00, 01, 00, 01, 00, 01},
                {01, 00, 01, 00, 01, 00, 01, 00},
                {00, 00, 00, 00, 00, 00, 00, 00},
                {00, 00, 00, 00, 00, 00, 00, 00},
                {00, 02, 00, 02, 00, 02, 00, 02},
                {02, 00, 02, 00, 02, 00, 02, 00},
                {00, 02, 00, 02, 00, 02, 00, 02},
            };

        public btnRetro()
        {
            InitializeComponent();
        }

        private void formLoad(object sender, EventArgs e)
        {
            cargarArrays();
        }

        public void posicionesActuales()
        {
            int[] x = new int[12];
            int[] y = new int[12];



            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    matrizDeTablero[i, j] = 0;

            for (int i = 0; i < 12; i++)
            {
                switch (i)
                {
                    case 00:
                        x[i] = roja1.Location.X;
                        y[i] = roja1.Location.Y;
                         break;
                    case 01:
                        x[i] = roja2.Location.X;
                        y[i] = roja2.Location.Y;
                         break;
                    case 02:
                        x[i] = roja3.Location.X;
                        y[i] = roja3.Location.Y;
                         break;
                    case 03:
                        x[i] = roja4.Location.X;
                        y[i] = roja4.Location.Y;
                         break;
                    case 04:
                        x[i] = roja5.Location.X;
                        y[i] = roja5.Location.Y;
                         break;
                    case 05:
                        x[i] = roja6.Location.X;
                        y[i] = roja6.Location.Y;
                         break;
                    case 06:
                        x[i] = roja7.Location.X;
                        y[i] = roja7.Location.Y;
                         break;
                    case 07:
                        x[i] = roja8.Location.X;
                        y[i] = roja8.Location.Y;
                         break;
                    case 08:
                        x[i] = roja9.Location.X;
                        y[i] = roja9.Location.Y;
                        break;
                    case 09:
                        x[i] = roja10.Location.X;
                        y[i] = roja10.Location.Y;
                        break;
                    case 10:
                        x[i] = roja11.Location.X;
                        y[i] = roja11.Location.Y;
                         break;
                    case 11:
                        x[i] = roja12.Location.X;
                        y[i] = roja12.Location.Y;
                         break;

                }
            }

            for(int i=0; i<x.Length; i++)
            {
                switch(x[i])
                {
                    case 50:
                        x[i] = 0;
                        break;
                    case 100:
                        x[i] = 1;
                        break;
                    case 150:
                        x[i] = 2;
                        break;
                    case 200:
                        x[i] = 3;
                        break;
                    case 250:
                        x[i] = 4;
                        break;
                    case 300:
                        x[i] = 5;
                        break;
                    case 350:
                        x[i] = 6;
                        break;
                    case 400:
                        x[i] = 7;
                        break;
                }
            }

            for(int i = 0; i < y.Length; i++)
            {
                switch (y[i])
                {
                    case 50:
                        y[i] = 0;
                        break;
                    case 100:
                        y[i] = 1;
                        break;
                    case 150:
                        y[i] = 2;
                        break;
                    case 200:
                        y[i] = 3;
                        break;
                    case 250:
                        y[i] = 4;
                        break;
                    case 300:
                        y[i] = 5;
                        break;
                    case 350:
                        y[i] = 6;
                        break;
                    case 400:
                        y[i] = 7;
                        break;
                }
            }

            //------------------------Falta codigo aqui      

            for (int i=0; i< 12; i++)
            {
                matrizDeTablero[y[i], x[i]] = 2;
            }

           

            for (int i = 0; i < 12; i++)
            {
                switch (i)
                {
                    case 00:
                        x[i] = azul1.Location.X;
                        y[i] = azul1.Location.Y;
                         break;
                    case 01:
                        x[i] = azul2.Location.X;
                        y[i] = azul2.Location.Y;
                         break;
                    case 02:
                        x[i] = azul3.Location.X;
                        y[i] = azul3.Location.Y;
                         break;
                    case 03:
                        x[i] = azul4.Location.X;
                        y[i] = azul4.Location.Y;
                         break;
                    case 04:
                        x[i] = azul5.Location.X;
                        y[i] = azul5.Location.Y;
                         break;
                    case 05:
                        x[i] = azul6.Location.X;
                        y[i] = azul6.Location.Y;
                         break;
                    case 06:
                        x[i] = azul7.Location.X;
                        y[i] = azul7.Location.Y;
                         break;
                    case 07:
                        x[i] = azul8.Location.X;
                        y[i] = azul8.Location.Y;
                         break;
                    case 08:
                        x[i] = azul9.Location.X;
                        y[i] = azul9.Location.Y;
                         break;
                    case 09:
                        x[i] = azul10.Location.X;
                        y[i] = azul10.Location.Y;
                         break;
                    case 10:
                        x[i] = azul11.Location.X;
                        y[i] = azul11.Location.Y;
                         break;
                    case 11:
                        x[i] = azul12.Location.X;
                        y[i] = azul12.Location.Y;
                         break;

                }
            }

                        for(int i=0; i<x.Length; i++)
            {
                switch(x[i])
                {
                    case 50:
                        x[i] = 0;
                        break;
                    case 100:
                        x[i] = 1;
                        break;
                    case 150:
                        x[i] = 2;
                        break;
                    case 200:
                        x[i] = 3;
                        break;
                    case 250:
                        x[i] = 4;
                        break;
                    case 300:
                        x[i] = 5;
                        break;
                    case 350:
                        x[i] = 6;
                        break;
                    case 400:
                        x[i] = 7;
                        break;
                }
            }

            for(int i = 0; i < y.Length; i++)
            {
                switch (y[i])
                {
                    case 50:
                        y[i] = 0;
                        break;
                    case 100:
                        y[i] = 1;
                        break;
                    case 150:
                        y[i] = 2;
                        break;
                    case 200:
                        y[i] = 3;
                        break;
                    case 250:
                        y[i] = 4;
                        break;
                    case 300:
                        y[i] = 5;
                        break;
                    case 350:
                        y[i] = 6;
                        break;
                    case 400:
                        y[i] = 7;
                        break;
                }
            }


            for (int i = 0; i < 12; i++)
            {
                matrizDeTablero[y[i], x[i]] = 1;
            }

            showMatrixTextBox.Text = string.Empty;

            for(int i=0; i<8; i++)
            {
                for(int j=0; j<8; j++)
                {
                    showMatrixTextBox.AppendText(matrizDeTablero[i,j].ToString());
                }
                showMatrixTextBox.AppendText("\n");
            }

            
        }

        public void ifqueen(PictureBox pcurrent, string pcolor)
        {
            if (pcolor == "azul" && pcurrent.Location.Y==400)
            {
                current.BackgroundImage = reaz.BackgroundImage;
                current.Tag = "queen";
            }
            else if (pcolor == "roja" && pcurrent.Location.Y == 50)
            {
                current.BackgroundImage = rero.BackgroundImage;
                current.Tag = "queen";
            }
        }
        /*Esta funcion carga la matriz con las fichas rojas y azules*/
        public void cargarArrays() //inicio cargar datos 
        {
            rojas = new object[][] // crea la matriz
            {
                new object[] {roja1,roja1.Location}, // crea objetos tipo arreglo
                new object[] {roja2,roja2.Location},
                new object[] {roja3,roja3.Location},
                new object[] {roja4,roja4.Location},
                new object[] {roja5,roja5.Location},
                new object[] {roja6,roja6.Location},
                new object[] {roja7,roja7.Location},
                new object[] {roja8,roja8.Location},
                new object[] {roja9,roja9.Location},
                new object[] {roja10,roja10.Location},
                new object[] {roja11,roja11.Location},
                new object[] {roja12,roja12.Location}
            };

            azules = new object[][]
            {
                new object[] {azul1,azul1.Location},
                new object[] {azul2,azul2.Location},
                new object[] {azul3,azul3.Location},
                new object[] {azul4,azul4.Location},
                new object[] {azul5,azul5.Location},
                new object[] {azul6,azul6.Location},
                new object[] {azul7,azul7.Location},
                new object[] {azul8,azul8.Location},
                new object[] {azul9,azul9.Location},
                new object[] {azul10,azul10.Location},
                new object[] {azul11,azul11.Location},
                new object[] {azul12,azul12.Location}
            };

            posicionesActuales();

        }//fin cargar datos


        public Point posicion(PictureBox cuadro)
        {
            return cuadro.Location;
        }

        public int promedio(int n1, int n2)
        {
            int result = n1 + n2;
            result = result / 2;
            return Math.Abs(result);
        }

        public string equivalencia(Point location)
        {
            string aux = "";

            switch (location.X)
            {
                case 50:
                    aux = "A";
                    break;
                case 100:
                    aux = "B";
                    break;
                case 150:
                    aux = "C";
                    break;
                case 200:
                    aux = "D";
                    break;
                case 250:
                    aux = "E";
                    break;
                case 300:
                    aux = "F";
                    break;
                case 350:
                    aux = "G";
                    break;
                case 400:
                    aux = "H";
                    break;
            }

            switch (location.Y)
            {
                case 50:
                    aux += "8";
                    break;
                case 100:
                    aux += "7";
                    break;
                case 150:
                    aux += "6";
                    break;
                case 200:
                    aux += "5";
                    break;
                case 250:
                    aux += "4";
                    break;
                case 300:
                    aux += "3";
                    break;
                case 350:
                    aux += "2";
                    break;
                case 400:
                    aux += "1";
                    break;
            }
            return aux;
        }

        public bool validacion(PictureBox origen, PictureBox destino, string color)
        {
            Point puntoOrigen = origen.Location;
            Point puntoDestino = destino.Location;

            if (((color == "azul") ? puntoDestino.Y - puntoOrigen.Y : puntoOrigen.Y - puntoDestino.Y) == 50 | (origen.Tag=="queen" && Math.Abs(puntoOrigen.Y - puntoDestino.Y)==50))
            {
                if (color == "azul") { blueKilled.Add(new object[] {"",""}); } else { redKilled.Add(null); }
                return true;
            }
            else if (((color == "azul") ? puntoDestino.Y - puntoOrigen.Y : puntoOrigen.Y - puntoDestino.Y) == 100 | (origen.Tag == "queen" && Math.Abs(puntoOrigen.Y - puntoDestino.Y) == 100))
            {
                Point puntoMedio = new Point(promedio(puntoDestino.X,puntoOrigen.X),promedio(puntoDestino.Y,puntoOrigen.Y));

                if (color == "azul")
                {
                    for (int i = 0; i < 12; i++)
                    {
                        if ((Point)rojas[i][1] == puntoMedio)
                        {
                            PictureBox martir = (PictureBox)rojas[i][0];
                            redKilled.Add(new object[]{martir,martir.Location});
                            martir.Location = new Point(0, 0);
                            martir.Visible = false;
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    for (int i = 0; i < 12; i++)
                    {
                        if ((Point)azules[i][1] == puntoMedio)
                        {
                            PictureBox martir = (PictureBox)azules[i][0];
                            blueKilled.Add(new object[] { martir, martir.Location });
                            martir.Location = new Point(0, 0);
                            martir.Visible = false;
                            return true;
                        }
                    }
                    return false;
                }
            }
            return false;
        }

        public bool extraMov(string pcolor)
        {
            Point ubicacion1; Point puntoMedio1;
            Point ubicacion2; Point puntoMedio2;
            bool libre1 = true;
            bool libre2 = true;

            if (pcolor == "azul" | current.Tag=="queen")
            {
                ubicacion1 = new Point(current.Location.X - 100, current.Location.Y + 100);
                puntoMedio1 = new Point(current.Location.X - 50, current.Location.Y + 50);

                ubicacion2 = new Point(current.Location.X + 100, current.Location.Y + 100);
                puntoMedio2 = new Point(current.Location.X + 50, current.Location.Y + 50);

                for (int i = 0; i < 12; i++)
                {
                    if ((Point)azules[i][1] == ubicacion1 | (Point)rojas[i][1] == ubicacion1)
                    {
                        libre1 = false;
                    }
                    if ((Point)azules[i][1] == ubicacion2 | (Point)rojas[i][1] == ubicacion2)
                    {
                        libre2 = false;
                    }
                }

                for (int i = 0; i < 12; i++)
                {
                    if (pcolor == "azul")
                    {
                        if (((Point)rojas[i][1] == puntoMedio1 && libre1 && ubicacion1.X <= 400 && ubicacion1.X >= 50 && ubicacion1.Y <= 400 && ubicacion1.Y >= 50) | ((Point)rojas[i][1] == puntoMedio2 && libre2 && ubicacion2.X <= 400 && ubicacion2.X >= 50 && ubicacion2.Y <= 400 && ubicacion2.Y >= 50))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (((Point)azules[i][1] == puntoMedio1 && libre1 && ubicacion1.X <= 400 && ubicacion1.X >= 50 && ubicacion1.Y <= 400 && ubicacion1.Y >= 50) | ((Point)azules[i][1] == puntoMedio2 && libre2 && ubicacion2.X <= 400 && ubicacion2.X >= 50 && ubicacion2.Y <= 400 && ubicacion2.Y >= 50))
                        {
                            return true;
                        }
                    }
                }
            }
            if (pcolor == "roja" | current.Tag == "queen")
            {
                libre1 = true;
                libre2 = true;
                ubicacion1 = new Point(current.Location.X - 100, current.Location.Y - 100);
                puntoMedio1 = new Point(current.Location.X - 50, current.Location.Y - 50);

                ubicacion2 = new Point(current.Location.X + 100, current.Location.Y - 100);
                puntoMedio2 = new Point(current.Location.X + 50, current.Location.Y - 50);

                for (int i = 0; i < 12; i++)
                {
                    if ((Point)azules[i][1] == ubicacion1 | (Point)rojas[i][1] == ubicacion1)
                    {
                        libre1 = false;
                    }
                    if ((Point)azules[i][1] == ubicacion2 | (Point)rojas[i][1] == ubicacion2)
                    {
                        libre2 = false;
                    }
                }

                for (int i = 0; i < 12; i++)
                {
                    if (pcolor == "roja")
                    {
                        if (((Point)azules[i][1] == puntoMedio1 && libre1 && ubicacion1.X <= 400 && ubicacion1.X >= 50 && ubicacion1.Y <= 400 && ubicacion1.Y >= 50) | ((Point)azules[i][1] == puntoMedio2 && libre2 && ubicacion2.X <= 400 && ubicacion2.X >= 50 && ubicacion2.Y <= 400 && ubicacion2.Y >= 50))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (((Point)rojas[i][1] == puntoMedio1 && libre1 && ubicacion1.X <= 400 && ubicacion1.X >= 50 && ubicacion1.Y <= 400 && ubicacion1.Y >= 50) | ((Point)rojas[i][1] == puntoMedio2 && libre2 && ubicacion2.X <= 400 && ubicacion2.X >= 50 && ubicacion2.Y <= 400 && ubicacion2.Y >= 50))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;

        }

        public void movimiento(object objeto, PictureBox pcurrent)
        {
            if (current != null)
            {
                string color = pcurrent.Name.ToString().Substring(0, 4);
                PictureBox cuadro = (PictureBox)objeto;

                if (validacion(pcurrent,cuadro,color))
                {
                    escribirBitacora(pcurrent, cuadro, color);
                    Point anterior = current.Location;
                    current.Location = cuadro.Location;
                    if (!extraMov(color) | Math.Abs(anterior.Y - cuadro.Location.Y) == 50)
                    {
                        ifqueen(current, color);
                        turno++;
                        current.BackColor = Color.Black;
                        current = null;
                        nextMov = false;
                    }
                    else
                    {
                        nextMov = true;
                    }
                    lblMov.Text = "Número de movimientos: " + turno.ToString();
                    cargarArrays();
                }
            }
        }

        public void seleccion(object objeto)
        {
            if (!nextMov)
            {
                try { current.BackColor = Color.Black; }
                catch { }
                PictureBox ficha = (PictureBox)objeto;
                current = ficha;
                current.BackColor = Color.Lime;
            }
        }

        public void escribirBitacora(PictureBox origen, PictureBox destino, string color)
        {
            if (color == "roja")
            {
                pilaRoja.Push(origen, origen.Location, destino.Location);
                txtBitacoraRoja.Text += equivalencia(origen.Location) + " - " + equivalencia(destino.Location) + Environment.NewLine;
            }
            else
            {
                pilaAzul.Push(origen,origen.Location,destino.Location);
                txtBitacoraAzul.Text += equivalencia(origen.Location) + " - " + equivalencia(destino.Location) + Environment.NewLine;
            }
        }

        private void A8_MouseClick(object sender, MouseEventArgs e)
        {
            movimiento(sender,current);
        }

        private void selectBlue(object sender, MouseEventArgs e)
        {
            if (turno % 2 == 1)
            {
                seleccion(sender);
            }
            else
            {
                MessageBox.Show("Toca turno del equipo ROJO", "Info");
            }
        }

        private void selectRed(object sender, MouseEventArgs e)
        {
            if (turno % 2 == 0)
            {
                seleccion(sender);
            }
            else
            {
                MessageBox.Show("Toca turno del equipo AZUL","Info");
            }
        }

        public void borrarBitacora(RichTextBox pbitacora)
        {
            List<string> myList = pbitacora.Lines.ToList();
            if (myList.Count > 0)
            {
                myList.RemoveAt(myList.Count - 2);
                pbitacora.Lines = myList.ToArray();
                pbitacora.Refresh();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (turno % 2 == 0)
            {
                pilaAzul.ultimaFicha().Location = pilaAzul.Pop();
                try
                {
                    PictureBox auxRed = (PictureBox)redKilled[redKilled.Count - 1][0];
                    auxRed.Location = (Point)redKilled[redKilled.Count - 1][1];
                    auxRed.Visible = true;
                } catch { }
                redKilled.RemoveAt(redKilled.Count-1);
                turno--;
                lblMov.Text = "Número de movimientos: " + turno.ToString();
                borrarBitacora(txtBitacoraAzul);
            }
            else
            {
                pilaRoja.ultimaFicha().Location = pilaRoja.Pop();
                try
                {
                    PictureBox auxBlue = (PictureBox)blueKilled[blueKilled.Count - 1][0];
                    auxBlue.Location = (Point)blueKilled[blueKilled.Count - 1][1];
                    auxBlue.Visible = true;
                } catch { }
                blueKilled.RemoveAt(blueKilled.Count - 1);
                turno--;
                lblMov.Text = "Número de movimientos: " + turno.ToString();
                borrarBitacora(txtBitacoraRoja);
            }
        }
    }
}
