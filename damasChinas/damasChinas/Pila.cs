﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace damasChinas
{
    class Pila
    {
        private Nodo inicio;

        public Pila()
        {
            Inicio = null;
        }

        public Nodo Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }

        public void Push(PictureBox ficha, Point origen, Point destino)
        {
            Nodo nuevo = new Nodo(ficha, origen, destino);
            if (Inicio == null)
            {
                Inicio = nuevo;
            }
            else
            {
                Nodo aux = IterarNodo(Inicio);
                aux.Siguiente = nuevo;
            }
        }

        public Point Pop()
        {
            Nodo aux;
            Point dato;

            aux = IterarNodo(inicio);
            dato = aux.Origen;

            return dato;
        }

        public PictureBox ultimaFicha()
        {
            Nodo aux;
            PictureBox ficha;

            aux = IterarNodo(inicio);
            ficha = aux.Ficha;

            return ficha;
        }

        private Nodo IterarNodo(Nodo Inicio)
        {
            if (Inicio.Siguiente == null)
            {
                return Inicio;
            }
            else
            {
                return IterarNodo(Inicio.Siguiente);
            }
        }
    }
}
