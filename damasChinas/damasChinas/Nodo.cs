﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace damasChinas
{
    class Nodo
    {
        private PictureBox ficha;
        private Point origen;
        private Point destino;
        private Nodo siguiente;

        public Nodo(PictureBox pFicha, Point pOrigen, Point pDestino)
        {
            Ficha = pFicha;
            Origen = pOrigen;
            Destino = pDestino;
            Siguiente = null;
        }

        public PictureBox Ficha
        {
            get { return ficha; }
            set { ficha = value; }
        }

        public Point Origen
        {
            get { return origen; }
            set { origen = value; }
        }

        public Point Destino
        {
            get { return destino; }
            set { destino = value; }
        }

        public Nodo Siguiente
        {
            get { return siguiente; }
            set { siguiente = value; }
        }
    }
}
